# picopower

This is picopower, the EE3012 final project by A.C., C.S., C.S., and J.Z.

This Raspberry Pi Pico-based device uses a magnetic sensing system to determine the current difference between two DC circuits.

Just for fun, it also plays the Bad Apple animation, which has been delta-compressed down to approx. 900 KiB. RLE compression was also implemented, but a subtle bug in the compression/decompression routines caused graphical corruption; thus, RLE is disabled in the final build.

## Structure

`src/` contains the C++ source, including the remnants of a SparkFun LCD interface library (MIT licensed) that was ported and heavily modified for use on the Pico.

`badapple.hpp` contains the Bad Apple animation in two arrays: the delta-compressed raw video, and the MP3-compressed audio. MP3 decompression using the RealNetworks Helix library was not implemented in time for submission, unfortunately. Please excuse the slightly messy code as a result.

The conversion tool `convertDelta.py` is written in Python3 and outputs the data as a binary file; it must then be parsed into a C header manually using `xxd` or similar.

## Building

You will need `g++-arm-none-eabi`. Build as you would any other Raspberry Pi Pico/cmake project. The Pico SDK should auto-fetch itself from Git if not already installed, but it is safest to install manually.

## License

This code, written by A.C. (compucat) is MIT licensed.

The Bad Apple animation is copyright Anira, et al, and is freely available online.
