#! /usr/bin/python3
from PIL import Image
import glob
import numpy as np
import sys

def bin2header(data, var_name='var'):
    out = []
    out.append('unsigned char {var_name}[] = {{'.format(var_name=var_name))
    l = [ data[i:i+12] for i in range(0, len(data), 12) ]
    for i, x in enumerate(l):
        line = ', '.join([ '0x{val:02x}'.format(val=ord(c)) for c in x ])
        out.append('  {line}{end_comma}'.format(line=line, end_comma=',' if i<len(l)-1 else ''))
    out.append('};')
    out.append('unsigned int {var_name}_len = {data_len};'.format(var_name=var_name, data_len=len(data)))
    return '\n'.join(out)


WIDTH = 84
HEIGHT = 48
def img_to_bytes(img: Image):
    thumbnail = img.resize((WIDTH, HEIGHT)).convert('1')
    matrix = np.array(thumbnail)
    data = bytes()
    for h in range(0, HEIGHT, 8):
        for w in range(WIDTH):
            bit = 0x00
            for i in range(8):
                dot = matrix[h + i, w]
                if dot:
                    continue
                bit |= 0x01 << i
            data += bit.to_bytes(1, 'big')
    assert(len(data) == WIDTH * HEIGHT / 8)
    return data

cmdchars = (ord('r'), ord('s'), ord('l'), ord('R'))
#Compression commands:
# 'r': Refresh display and reset for start of new frame.
# 's'(n): Skip next n bytes, up to 255
# 'l'(b): Write literal next byte to display and advance framebuffer address.
# 'R'(b, c): Write c copies of byte to framebuffer
# Any other byte: write literal byte to display and advance framebuffer address.

rle = False #Disable RLE encoding due to a subtle bug

bytesout = bytearray()
lastframe = "none yet" #Uses a typecheck to write a keyframe only on the first frame.
for i in range(1,13150):
	im = Image.open("pngs/png_"+str(i)+".png")
	raw = img_to_bytes(im)
	bytesout.extend('r'.encode()) #Reset/refresh display.
	skipped = 0
	p = 0
	while p < len(raw):
		#Check for a skippable byte.
		if (type(lastframe) is bytes) and (raw[p] == lastframe[p]): skipped+=1
		else:
			#Write skip command if bytes were skipped.
			while skipped > 0:
				bytesout.extend('s'.encode())
				bytesout.extend(min(skipped, 255).to_bytes(length=1, byteorder='big'))
				skipped -= min(skipped, 255)
			#Guess we're stuck writing a literal...check if it's RLE'able
			rlecount = 0
			while (p+rlecount < len(raw)) and (raw[p] == raw[p+rlecount]): rlecount += 1
			if rlecount > 3 and rle == True:
				#print("rle! "+str(rlecount))
				towrite = raw[p].to_bytes(length=1, byteorder='big')
				rlecount -=1
				p += rlecount
				while (rlecount > 0):
					bytesout.extend('R'.encode())
					bytesout += towrite
					bytesout += min(rlecount, 255).to_bytes(length=1, byteorder='big')
					rlecount -= min(rlecount, 255)
			else: #No RLE possible.
				if raw[p] in cmdchars: bytesout.extend('l'.encode())
				bytesout += raw[p].to_bytes(length=1, byteorder='big')
		p+=1
	lastframe = raw
	im.close()
	if (i%100 == 0) : print(i)
with open("out_binary_delta", 'wb') as f: f.write(bytesout)
