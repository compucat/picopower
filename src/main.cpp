/**
* picoPower main file
*
*
*/

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include "pico.h"
#include "pico/stdlib.h"
#include "hardware/irq.h"
#include "hardware/address_mapped.h"
#include "hardware/spi.h"
#include "hardware/adc.h"
#include "hardware/pwm.h"
#include "hardware/sync.h"
#include <unistd.h>

#include "pico/bootrom.h"

#include "lcd.hpp"
#include "badapple.hpp"

#define min(x,y) (!(y<x)?x:y)

using namespace std;

const char* greetz = "             This is picoPower, the EE 3012 power meter. Greetings to UMN students, faculty, staff, and all the friends we inevitably forgot. We're no strangers to love. You know the rules, and so do I. The rules say we must rickroll you. The rules are a subset of the laws, which include all known laws of aviation, which say there is no way a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies anyway, because bees don't care what humans think is impossible. / I'm running out of silly text to put here, so this is Alex signing off. / ";

//Variables for input averaging
float voltageQueue[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int qPtr = 0;

//Grab a new ADC value, convert to voltage, and return average of last 16 reads
//v=0.0083i - 0.0255
float getAvgVoltage() {
	voltageQueue[qPtr] = (adc_read() * (3.3f/ (1 << 12)));
	float avg=0;
	for(int i=0; i<16; i++) avg+=voltageQueue[i];
	qPtr = (qPtr+1)%16;
	return (avg/16.0);
}

float posOffset = - 0.0433;
float negOffset = -0.0276;

#if defined(OLDSENSOR)
//Returns current in milliamps.
float mapToCurrent(float v) {
	return ((v + 0.0255)/0.0083);
}
#else
float mapToCurrent(float v) {
	return ((v + posOffset)/0.0099);
}
#endif

float mapToCurrentNeg(float v) {
	return ((v+negOffset)/0.0096);
}

int main() {
	//Initialization.
  stdio_init_all();
  gpio_init(25);
  gpio_set_dir(25, GPIO_OUT);

  initLCD(); //lcd pins hardcoded in lcd.hpp. not best practice but oh well
	updateDisplay();
	gotoXY(0,0);

	adc_init();
  adc_gpio_init(26);
  adc_select_input(0);

	//calibrate

	for(int i=1500; i>=0; i--) {
		clearDisplay(0);
		char adc[16];
		sprintf(adc, "Please input");
		setStr(adc, 0, 0, 1);
		sprintf(adc, "+50mA offset");
		setStr(adc, 0, 8, 1);
		sprintf(adc, "to calibrate");
		setStr(adc, 0, 16, 1);
		sprintf(adc, "%.3f", (i/100.0));
		setStr(adc, 0, 24, 1);
		updateDisplay();
		sleep_ms(10);
	}
	posOffset = (0.0099 * 50) - getAvgVoltage();

	/*for(int i=1500; i>=0; i--) {
		clearDisplay(0);
		char adc[16];
		sprintf(adc, "Please input");
		setStr(adc, 0, 0, 1);
		sprintf(adc, "-50mA offset");
		setStr(adc, 0, 8, 1);
		sprintf(adc, "%f V", getAvgVoltage());
		setStr(adc, 0, 16, 1);
		sprintf(adc, "%.3f", (i/100.0));
		setStr(adc, 0, 24, 1);
		updateDisplay();
		sleep_ms(10);
	}
	negOffset = (0.0096 * 50) - getAvgVoltage();*/

	// //BEGIN TREMOR OGG AUDIO TEST
	// // set_sys_clock_khz(131000, true);//131072, true); //Sets up for 16khz with 8x oversampling
	// // gpio_set_function(audioPin, GPIO_FUNC_PWM);
	// // uint slice_num = pwm_gpio_to_slice_num(audioPin);
	// // pwm_set_wrap(slice_num, 256);
	// // //pwm_set_gpio_level(audioPin, 128);
	// // pwm_set_clkdiv(slice_num, 4);
	// // pwm_clear_irq(slice_num);
	// // pwm_set_irq_enabled(slice_num, true);
	// // //irq_set_exclusive_handler(PWM_IRQ_WRAP, on_pwm_wrap);
	// // irq_set_enabled(PWM_IRQ_WRAP, true);
	// //
	// // pwm_set_enabled(slice_num, true);
	//
	// Adafruit_MP3 player;
	//
	// currentPtr = (uint8_t*) badappleaudio;
	// thisManyBytesLeft = sizeof(badappleaudio);
	//
	// player.begin();
	//
	// player.setSampleReadyCallback(write_pwm);
	// player.setBufferCallback(getMoreData);
	// player.play();
	//
	// for(;;) {
	// 	player.tick();
	// }
	// //END TREMOR OGG AUDIO TEST


	//Variables for scroller and video decoder
	int xoff=0, chroff=0, slowdown=0;
	int fbramPtr=0;

	//Forever, iterate over the video data.
	for(;;) for(int x=0; x<sizeof(badapplevideo); x++){
		uint8_t b = badapplevideo[x];
		switch(b) { //Parse video data
			case 'r': //Refresh display and reset for new frame draw. Grab ADC while we're at it.
			{
				char adc[16];
				float avg = getAvgVoltage();
				float avgCurrent = mapToCurrent(avg);
				setLine(&greetz[chroff], xoff, 0, 1);
				//sprintf(adc, "avg: %.3f V", avg);
    		//setStr(adc, 0, 8, 1);
				sprintf(adc, "%7.3f mA%c   ", avgCurrent, ((avgCurrent) < 10 ? '?' : '.'));
    		setStr(adc, 0, 40, 1);
				//sprintf(adc, "neg: %.3f mA%c", mapToCurrentNeg(avg), ((avgCurrent) < 10 ? '?' : '.'));
    		//setStr(adc, 0, 40, 1);

				//Handle scroller logic
				if((++slowdown)>3) {
					slowdown=0;
					xoff--;
					if(xoff<-5) {
						xoff=0;
						chroff = (chroff+1) % strlen(greetz);
					}
				}

				updateDisplay();

				//Check for serial reset, since I don't have an SWD probe connected.
				char c = getchar_timeout_us(0);
				if (c != PICO_ERROR_TIMEOUT && c == 'R') reset_usb_boot(25, 0);

				//Delay for approx 60fps.
				sleep_ms(16);
				fbramPtr=0;
				break;
			}
			case 's': //skip up to 256 bytes
				fbramPtr+=badapplevideo[++x];
				break;
			case 'l': //literal next byte
				displayMap[fbramPtr++] = badapplevideo[++x];
				break;
			//RLE decoding mostly-works, but there's some funny corruption. I can't
			//tell if it's on the encoding or decoding side.
			//Disabled for now so it doesn't screw up the non-RLE version.
			/*case 'R': {
				uint8_t val = badapple[++x];
				int len = badapple[++x];
				//memset(&displayMap[i], len, val);
				//i+=len;
				for(;len>0;len--) displayMap[i++] = val;
				break;
			}*/
			default: //literal byte
				displayMap[fbramPtr++] = b;
		}
		if(fbramPtr >= 504) fbramPtr = fbramPtr % 504; //failsafe
	}
}
